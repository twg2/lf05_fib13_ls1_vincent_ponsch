import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {

		// -----------
		// Benutzereingaben lesen
		// Methode Artikel
		String artikel = liesString("Was m�chten Sie bestellen?");
		
		// Methode Anzahl
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		
		// Methode Preis
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		
		// Methode Mehrwertsteuer
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		// -----------
		// Verarbeiten
		// Methode Gesamtnettopreis
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		
		// Methode Gesamtbruttopreis
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// -----------
		// Ausgeben
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);


	}
	// -----------
	// Methoden Eingabe
	// Methode Artikel
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String a = myScanner.next();
		return a;
	}
	// Methode Anzahl
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int b = myScanner.nextInt();
		return b;
	}
	// Methode Preis / Mehrwertsteuer
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double c = myScanner.nextDouble();
		return c;
	}

	
	// -----------
	// Methoden Verarbeitung
	// Methode Gesamtnettopreis
	public static double berechneGesamtnettopreis(int an, double n) {
		return an * n;	
	}
	// Methode Gesamtbruttopreis
	public static double berechneGesamtbruttopreis (double n, double m) {
		return n * (1 + m / 100);
	}
	
	// -----------
	// Methoden Ausgabe
	// Methode Rechnung ausgeben
	public static void rechnungausgeben(String a, int an, double n, double b, double m) {
		System.out.println("Ihre Rechnung");
		System.out.printf("\t Nettopreis:  %-10s %6d %10.2f %n", a, an, n);
		System.out.printf("\t Bruttopreis: %-10s %6d %10.2f (%.1f%s)%n", a, an, b, m, "% Mehrwertsteuer");
	}

}
