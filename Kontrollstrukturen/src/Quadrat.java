// Vincent P�nsch | FI-B 13

import java.util.Scanner;

public class Quadrat {

	public static void main(String[] args) {
		int groe�e;
		Scanner myScanner = new Scanner(System.in);
		
		// Abfrage des UserInputs
		System.out.printf("Sie geben diesem Programm eine Zahl und es gibt Ihnen ein Quadrat in entsprechender Gr��e aus. %n"
				+ "Bitte geben Sie eine Zahl ein: ");
		groe�e = myScanner.nextInt();
		
		// --------------------
		// Quadrat - Ausgabe
		// Ausgabe - oben
		for(int a = 1; a <= groe�e; a++) {
			System.out.print("*  ");
		}
		System.out.println("");
		// Ausgabe - mitte / Seiten
		for(int a = 1; a <= groe�e - 2; a++) {
			System.out.print("*  ");
			for(int b = 1; b <= groe�e -2 ; b++) {
				System.out.print("   ");
			}
			System.out.println("*  ");
		}
		//Ausgabe - unten
		for(int a = 1; a <= groe�e; a++) {
			System.out.print("*  ");
		}
		// es fehlen minimale Abst�nde zum Quadrat?

	}

}
