// Vincent P�nsch | FI-B 13

import java.util.Scanner;

public class Million {

	public static void main(String[] args) {
		// Deklaration Variablen
		double einlage;
		double zinssatz;
		double summe = 0;
		int jahreszahl = 0;
		String eingabe;
		Scanner myScanner = new Scanner(System.in);
		
		// Abrage UserInput
		System.out.printf("Dies ist ein Programm, welches berechnet wie lange Sie brauchen um Million�r zu werden.%n"
				+ "Zum Beenden des Programms geben Sie bitte den Buchstaben \"n\" ein. "
				+ "Zum Wiederholen des Programms geben Sie bitte den Buchstaben \"j\" ein.");
		eingabe = myScanner.next();
		
		// ----------------
		// Rechnung
		while(eingabe != "n" && eingabe.equals("j")) {
			System.out.println("Bitte geben Sie die H�he der Einlage ein: ");
			einlage = myScanner.nextDouble();
			System.out.println("Bitte geben Sie die H�he des Zinssatzes ein: ");
			zinssatz = myScanner.nextDouble();
			zinssatz = zinssatz / 100;
			summe = einlage;
			
			while(summe < 1000000) {
				summe += summe * zinssatz;
				jahreszahl++;
			}
			// Ergebnis und Wiederholung
			System.out.println("Die Dauer bis Sie Million�r werden, betr�gt " + jahreszahl + " Jahre.");
			System.out.println("M�chten Sie die Rechnung erneut durchf�hren?");
			eingabe = myScanner.next();
			if(eingabe.equals("n")) {
				break;
			}
			else if (eingabe.equals("j")) {
				
			}
			else {
				System.out.println("Die Eingabe war ung�ltig.");
				break;
			}
		}

	}

}
