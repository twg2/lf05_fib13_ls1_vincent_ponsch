// Vincent P�nsch | FI-B 13

import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		
		// Variablen- und Scannerdeklaration
		int gewicht;
		int groe�e;
		String geschlecht;
		
		Scanner myScanner = new Scanner(System.in);
		
		// Abfrage der Userinformationen
		System.out.println("Bitte geben Sie Ihr Gewicht ein [in kg]: ");
		gewicht = myScanner.nextInt();
		System.out.println("Bitte geben Sie Ihre Gr��e ein [in cm]: ");
		groe�e = myScanner.nextInt();
		System.out.println("Bitte geben Sie Ihr Geschlecht ein [m / w]: ");
		geschlecht = myScanner.next();
		
		// Berechnung BMI - m�nnlich
		if(geschlecht.equals("m")) {		// .equals() wird genutzt da == nicht funktioniert? vllt aber auch nur Denkfehler meinerseits
			if(bmi(gewicht, groe�e) < 20) {
				System.out.println("Sie sind untergewichtig. Ihr BMI betr�gt " + Math.round(bmi(gewicht, groe�e)) + ".");		// Math.round() liefert eine genauere Angabe als es mit printf() abzuschneiden
			}
			else if(bmi(gewicht, groe�e) >= 20 && bmi(gewicht, groe�e) <= 25) {
				System.out.println("Sie liegen im Normalgewicht. Ihr BMI betr�gt " + Math.round(bmi(gewicht, groe�e)) + ".");
			}
			else {
				System.out.print("Sie sind �bergewichtig. Ihr BMI betr�gt " + Math.round(bmi(gewicht, groe�e)) + ".");
			}
		}
		// Berechnung BMI - weiblich
		else if(geschlecht.equals("w")) {
			if(bmi(gewicht, groe�e) < 19) {
				System.out.println("Sie sind untergewichtig. Ihr BMI betr�gt " + Math.round(bmi(gewicht, groe�e)) + ".");
			}
			else if(bmi(gewicht, groe�e) >= 19 && bmi(gewicht, groe�e) <= 24) {
				System.out.println("Sie liegen im Normalgewicht. Ihr BMI betr�gt " + Math.round(bmi(gewicht, groe�e)) + ".");
			}
			else {
				System.out.print("Sie sind �bergewichtig. Ihr BMI betr�gt " + Math.round(bmi(gewicht, groe�e)) + ".");
			}
		}
		else {
			System.out.println("Geben Sie bitte einen vom Programm akzeptierten Buchstaben ein. Vielen Dank.");
		}
		
		myScanner.close();

	}

	public static double bmi(double a, double b){
		double erg;
		double c;
		c = b / 100;
		erg = a / (c * c);
		return erg;
	}
}
