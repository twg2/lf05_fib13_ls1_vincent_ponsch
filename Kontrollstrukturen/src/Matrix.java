// Vincent P�nsch | FI-B 13

import java.util.Scanner;

public class Matrix {

	public static void main(String[] args) {
		
		// Deklaration ben�tigter Variablen
		int eingabe;
		int a = 1;
		int b = 1;
		int zahl1;
		int zahl2;
		int zahl3;
		
		// Abfrage UserInput
		Scanner myScanner = new Scanner(System.in);
		
		System.out.printf("Dieses Programm zeigt Ihnen eine Matrix des kleinen Ein-mal-Eins an.%n"
				+ "Alle Zahlen, welche die eingebene Ziffer enthalten, durch diese ohne Rest teilbar sind oder ihrer Quersumme entsprechen, werden mit einem Stern ersetzt.%n"
				+ "Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		eingabe = myScanner.nextInt();
		System.out.print("0 ");
		
		// --------------
		// "Rechnung"
		if (eingabe >= 2 && eingabe <= 9) {
			// Test ob Zahl ein Vielfaches der Eingabe ist
			while(a < 100 && b < 100) {
				zahl1 = a / 10;
				zahl2 = a % 10;
				zahl3 = zahl1 + zahl2;
				if(b % 10 == 0) {
					System.out.println(" ");
				}
				
				// Ersetzen bei Teilbarkeit ohne Rest
				if(a % eingabe == 0) {
					System.out.print("* ");
				}
				
				else if(zahl3  == eingabe) {
					System.out.print("* ");
				}
				
				else if (a / 10 == eingabe) {
					System.out.print("* ");
				}
				
				else if (a % 10 == eingabe) {
					System.out.print("* ");
				}
				
				// wird nicht ersetzt
				else {
					System.out.print(a + " ");
				}
				
				// Erh�hen
				a = a + 1;
				b = b + 1;
			}
		
		}

	}

}
