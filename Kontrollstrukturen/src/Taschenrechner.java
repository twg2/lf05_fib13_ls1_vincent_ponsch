// Vincent P�nsch | FI-B 13

import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {
		// Willkommen und Abfrage des UserInputs
		// Deklaration Scanner
		Scanner myScanner = new Scanner(System.in);
		
		// Deklaration Variablen
		int zahl1;
		int zahl2;
		int ergebnis;
		String operator;
		
		// Abfrage UserInput
		// Zahl 1
		System.out.printf("Willkommen zu diesem umwerfenden Taschenrechner!%n"
				+ "Sie k�nnen hier mit den vier Grundrechenarten und 2 Ganzzahlen arbeiten.%n"
				+ "Bitte geben Sie die erste Zahl ein: %n");
		zahl1 = myScanner.nextInt();
		// Zahl 2
		System.out.println("Bitte geben Sie nun die zweite Zahl ein: ");
		zahl2 = myScanner.nextInt();
		// Operator
		System.out.printf("M�chten Sie addieren (+), subtrahieren (-), multiplizieren (*) oder dividieren (/) ?%n"
				+ "Bitte geben Sie den gew�nschten Operator ein: %n");
		operator = myScanner.next();
		
		// Rechnung; Switch zur Auswahl des Operators
		switch(operator) {
		case "+":
			ergebnis = zahl1 + zahl2;
			System.out.print("Ergebnis: " + ergebnis);
			break;
		case "-":
			ergebnis = zahl1 - zahl2;
			System.out.print("Ergebnis: " + ergebnis);
			break;
		case "*":
			ergebnis = zahl1 * zahl2;
			System.out.print("Ergebnis: " + ergebnis);
			break;
		case "/":
			ergebnis = zahl1 / zahl2;
			System.out.print("Ergebnis: " + ergebnis);
			break;
		default:
			System.out.print("Bitte geben Sie einen g�ltigen Operator ein!%n"
					+ "Da der Taschenrechner dann doch nicht so umwerfend ist, m�ssen Sie ihn nun neu starten (:");
			break;
		}
		
		myScanner.close();
		
	}

}

