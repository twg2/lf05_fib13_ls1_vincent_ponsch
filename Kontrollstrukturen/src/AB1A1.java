
public class AB1A1 {

	public static void main(String[] args) {
		int zahl1 = 5;
		int zahl2 = 9;
		int zahl3 = 22;
		
		// --------
		// Eingabe von 2 Zahlen
		// Meldung bei gleicher Zahl
		if (zahl1 == zahl2) {
			System.out.println("gleich!");
		}
		else {
			System.out.println("was anderes.");
		}
		
		// Meldung wenn zahl2 >
		if (zahl1 < zahl2) {
			System.out.println("zweite zahl ist gr��er.");
		}
		else {
			System.out.println("zweite zahl ist nicht gr��er.");
		}
		
		// Meldung wenn zahl1 >=
		if (zahl1 >= zahl2) {
			System.out.println("zahl1 ist gr��er oder gleich zahl2.");
		}
		else {
			System.out.println("zahl1 ist nicht gr��er oder gleich zahl2.");
		}
		
		// --------
		// Eingabe von 3 Zahlen
		// Meldung wenn zahl1 am gr��ten
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("zahl1 ist am g��ten.");
		}
		else {
			System.out.println("zahl1 ist nicht am gr��ten.");
		}
		
		// Meldung wenn zahl3 am gr��ten
		if (zahl3 > zahl1 || zahl3 > zahl2) {
			System.out.println("zahl3 ist am gr��ten.");
		}
		else {
			System.out.println("zahl3 ist nicht am gr��ten.");
		}
		
		// Meldung gr��te zahl
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("zahl1 ist am g��ten.");
		}
		else if (zahl2 > zahl1 && zahl2 > zahl3) {
			System.out.println("zahl2 ist am g��ten.");
		}
		else {
			System.out.println("zahl3 ist am g��ten.");
		}
		
	}

}
