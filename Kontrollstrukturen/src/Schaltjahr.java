// Vincent P�nsch | FI-B 13

import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		// UserInput
		int year;
		Scanner myScanner = new Scanner(System.in);
		System.out.printf("Sie m�chten �berpr�fen ob ein Jahr ein Schaltjahr ist?%nBitte geben Sie eine Jahreszahl ein: ");
		year = myScanner.nextInt();
		
		// Pr�fung ob Schaltjahr ja / nein
		if(year % 4 != 0) {
			System.out.print("Kein Schaltjahr :(");
		}
		else if(year % 100 != 0) {
			System.out.print("Schaltjahr!");
		}
		else if(year % 400 != 0) {
			System.out.print("Kein Schaltjahr :(");
		}
		else {
			System.out.print("Schaltjahr");
		}

	}

}
