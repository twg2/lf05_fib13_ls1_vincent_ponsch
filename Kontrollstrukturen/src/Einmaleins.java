// Vincent P�nsch | FI-B 13

public class Einmaleins {

	public static void main(String[] args) {
		// Z�hler f�r erste Zahl (1*b, 2*b, usw.)
		for(int a = 1; a <= 10; a++) {
			// Z�hler f�r zweite Zahl (a*1, a*2, usw.)
			for(int b = 1; b <= 10; b++) {
				int ergebnis = a * b;
				System.out.print(ergebnis + " ");
			}
			// Einteilung in "10er Bl�cke"
			System.out.printf("%n");
		}

	}

}