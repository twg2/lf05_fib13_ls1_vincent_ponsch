// Vincent P�nsch | FI-B 13

import java.util.Scanner;

public class OhmschesGesetz {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		double r;
		double u;
		double i;
		String gesucht;
		
		System.out.printf("Herzlich Willkommen. Dies ist ein Rechner f�r das Ohmsche Gesetz.%n"
				+ "Bitte geben Sie den Wert ein, den Sie berechnen m�chten. G�ltig sind U, I und R: %n");
		gesucht = myScanner.next();
		
		switch(gesucht) {
		case "R":
			System.out.printf("Sie suchen also den Widerstand. Geben Sie nun bitte den Wert f�r U (Spannung) ein: %n");
			u = myScanner.nextDouble();
			System.out.printf("Geben Sie bitte die Stromst�rke ein: %n");
			i = myScanner.nextDouble();
			r = u / i;
			System.out.printf("Der Widerstand betr�gt " + r + " Ohm.%n");
		case "U":
			System.out.printf("Sie suchen also die Spannung. Geben Sie nun bitte den Wert f�r R (Widerstand) ein: %n");
			r = myScanner.nextDouble();
			System.out.printf("Geben Sie bitte die Stromst�rke ein: %n");
			i = myScanner.nextDouble();
			u = r * i;
			System.out.printf("Die Spannung betr�gt " + u + " Volt.%n");
		case "I":
			System.out.printf("Sie suchen also die Stromst�rke. Geben Sie nn bitte den Wert f�r U (Spannung) ein: %n");
			u = myScanner.nextDouble();
			System.out.printf("Geben Sie bitte den Widerstand ein: %n");
			r = myScanner.nextDouble();
			i = u / r;
			System.out.printf("Die Stromst�rke betr�gt " + i + " Ampere.%n");
		default:
			System.out.printf("Geben Sie bitte ein g�ltiges Formelzeichen ein!%n"
					+ "Sie m�ssen den Rechner nun neu starten.");
		}
		
		myScanner.close();

	}

}
