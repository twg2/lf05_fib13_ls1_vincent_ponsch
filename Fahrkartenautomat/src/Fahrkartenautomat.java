﻿//Vincent Pönsch - Klasse FI-B 13

import java.util.Scanner;

class Fahrkartenautomat
{
	public static void main(String[] args) {
    	
		double rueckgabebetrag;
		
		/*Hier wird ein Array zum Einsparen einer Methode verwendet.
		  beides = zuZahlenderBetrag[0], anzahlKarten[1]*/
		double beides[] = fahrkartenbestellungErfassen(); 
		rueckgabebetrag = fahrkartenBezahlen(beides[0]);
		fahrkartenAusgeben(beides[1]);
		rueckgeldAusgeben(rueckgabebetrag, beides[1]);

	}
	
	// Methode Fahrkartenbestellung
	// ----------------------------	
	public static double[] fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.printf("Guten Tag!%nEin Ticket kostet hier 3,50 Euro.%nWie viele Fahrkarten möchten Sie kaufen? %nBitte geben Sie die Anzahl ein: ");
		
		double zuZahlenderBetrag;
		final double KARTENPREIS = 3.5; //Kartenfestpreis
		double anzahlKarten;
		
		anzahlKarten = tastatur.nextInt();
		System.out.printf("%nSie kaufen " + anzahlKarten + " Fahrkarten.%n");
		System.out.printf("Zu zahlender Betrag (EURO): " + KARTENPREIS * anzahlKarten + "%n");
		zuZahlenderBetrag = KARTENPREIS * anzahlKarten;
		
		return new double[] {zuZahlenderBetrag, anzahlKarten};	
	}
	// Methode Fahrkarten bezahlen
	// ---------------------------
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		Scanner tastatur = new Scanner(System.in);
		double eingeworfeneMünze;
		
		System.out.printf("Noch zu zahlen: " + zuZahlenderBetrag + " Euro%nZahlen Sie bitte in mind. 5ct bis max. 2 Euro Münzen.%nMünzeinwurf:");
		while(zuZahlenderBetrag > 0.0)
			{
			eingeworfeneMünze = tastatur.nextDouble();
			zuZahlenderBetrag = zuZahlenderBetrag - eingeworfeneMünze;
			if(zuZahlenderBetrag > 0.0)
				{
				System.out.printf("Noch zu zahlen: %.2f%nMünzeinwurf:", zuZahlenderBetrag);
				}
			}
		if(zuZahlenderBetrag < 0.0)
		{
			zuZahlenderBetrag = (zuZahlenderBetrag * -1);
			System.out.printf("Ihnen werden " + zuZahlenderBetrag + " Euro ausgegeben.");
		}
		
		return zuZahlenderBetrag;
	}
	// Methode Fahrkarten ausgeben
	// ---------------------------
	public static void fahrkartenAusgeben(double anzahlKarten) {
    	if(anzahlKarten < 2) {										//if - else soll zwischen 1 oder mehr karten unterscheiden
    		System.out.println("\nFahrschein wird ausgegeben");		
    		for (int i = 0; i < 8; i++)								//hier wird 1 karte gekauft
    			{														
    			System.out.print("=");
    			try {
    				Thread.sleep(250);
    				} catch (InterruptedException e) {
    					e.printStackTrace();
    					}
    			}
    		}
    	else {
    		System.out.println("\nFahrscheine werden ausgegeben");
    		for (int i = 0; i < 8; i++)								//hier werden >1 karte gekauft
    			{
    			System.out.print("=");
    			try {
    				Thread.sleep(250);
    				} catch (InterruptedException e) {
    					e.printStackTrace();
    				}
    			}
    		}
    	System.out.println("\n");
	}
	// Methode Rückgeld ausgeben
	// -------------------------
	public static void rueckgeldAusgeben(double rueckgabebetrag, double anzahlKarten) {
    	if(rueckgabebetrag > 0.0)
    	{
    		System.out.printf("Der Rückgabebetrag in Höhe von %.2f €", rueckgabebetrag);
    		System.out.println(" wird in folgenden Münzen ausgezahlt:");
    		
    		while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
    			{
    			System.out.println("2 €");
    			rueckgabebetrag -= 2.0;
    			}
    		while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
    			{
    			System.out.println("1 €");
    			rueckgabebetrag -= 1.0;
    			}
    		while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
    			{
    			System.out.println("50 ct");
    			rueckgabebetrag -= 0.5;
    			}
    		while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
    			{
    			System.out.println("20 ct");
    			rueckgabebetrag -= 0.2;
    			}
    		while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
    			{
    			System.out.println("10 ct");
    			rueckgabebetrag -= 0.1;
    			}
    		while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
    			{
    			System.out.println("5 ct");
    			rueckgabebetrag -= 0.05;
    			}
    		}
    	if (anzahlKarten <= 1) {
    		System.out.printf("\nVergessen Sie nicht, den Fahrschein");
    	}
    	else {
    		System.out.printf("\nVergessen Sie nicht, die Fahrscheine");
    	}
    	
    	System.out.println(" vor Fahrtantritt entwerten zu lassen!\n"+
    						"Wir wünschen Ihnen eine gute Fahrt.");
    	}
	}