import java.util.Scanner; // Import der Klasse Scanner 
 
public class einfacherRechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    float zahl1 = myScanner.nextFloat();  
     
    System.out.print("Bitte geben Sie eine zweite Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    float zahl2 = myScanner.nextFloat();  
     
    // Die Rechenoperation der Variablen zahl1 und zahl2  
    // wird der entsprechenden "ergebnis"-Variable zugewiesen. 
    float ergebnis = zahl1 + zahl2;
    float ergebnis2 = zahl1 - zahl2;
    float ergebnis3 = zahl1 * zahl2;
    float ergebnis4 = zahl1 / zahl2;
     
    
    //für Übersichtlichkeit wird nach 2 Nachkommastellen gecutted
    //Addition
    System.out.print("\n\nErgebnis der Addition lautet: "); 
    System.out.printf(zahl1 + " + " + zahl2 + " = %.2f%n", ergebnis);
    //Subtrktion
    System.out.print("\nErgebnis der Subtraktion lautet: ");
    System.out.printf(zahl1 + " - " + zahl2 + " = %.2f%n", ergebnis2);
    //Multiplikation
    System.out.print("\nErgebnis der Multiplikation lautet: ");
    System.out.printf(zahl1 + " * " + zahl2 + " = %.2f%n", ergebnis3);
    //Division
    System.out.print("\nErgebnis der Division lautet: ");
    System.out.printf(zahl1 + " / " + zahl2 + " = %.2f", ergebnis4);
 
    myScanner.close(); 
    
  }    
}