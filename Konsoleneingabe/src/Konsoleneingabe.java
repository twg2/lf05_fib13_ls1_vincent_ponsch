import java.util.Scanner;

public class Konsoleneingabe {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.printf("Geben Sie bitte eine Ganzzahl ein: ");
		
		int ganzzahl = myScanner.nextInt();
		
		System.out.println("Ihre eingegebene Ganzzahl lautet " + ganzzahl + ".");

		myScanner.close();
	}

}
