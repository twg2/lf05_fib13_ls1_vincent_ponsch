
public class Aufgabe1 {

	public static void main(String[] args) {
		
		String s = "*";
		
		System.out.printf("%4s", s);
		System.out.printf("%s\n", s);
		
		System.out.printf("%s", s);
		System.out.printf("%7s\n", s);
		System.out.printf("%s", s);
		System.out.printf("%7s\n", s);
		
		System.out.printf("%4s", s);
		System.out.printf("%s\n", s);
	
	}
	
}
