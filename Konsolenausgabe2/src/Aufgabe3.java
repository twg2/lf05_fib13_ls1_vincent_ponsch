
// Vincent Louis P�nsch, FI-B 13

public class Aufgabe3 {

	public static void main(String[] args) {
		
		//Definition der Variablen
		
		String F = "Fahrenheit" ;
		String C = "Celsius" ;
		
		String a = "-20" ;
		double b = -28.8889 ;
		String c = "-10" ;
		double d = -23.3333 ;
		String e = "0" ;
		double f = -17.7778 ;
		String g = "20" ;
		double h = -6.6667 ;
		String i = "30" ;
		double j = -1.1111 ;
		
		//Ausgabe
		
		System.out.printf("%-12s|", F) ;
		System.out.printf("%10s%n", C) ;		 //ungleiche Abst�nde? (\�.�/)
		System.out.printf("------------------------%n") ;
		
		System.out.printf("%-12s|", a) ;
		System.out.printf("%10.2f%n", b) ;
		
		System.out.printf("%-12s|", c) ;
		System.out.printf("%10.2f%n", d) ;
		
		System.out.printf("+%-11s|", e) ;
		System.out.printf("%10.2f%n", f) ;
		
		System.out.printf("+%-11s|", g) ;
		System.out.printf("%10.2f%n", h) ;
		
		System.out.printf("+%-11s|", i) ;
		System.out.printf("%10.2f%n", j) ;

	}

}
