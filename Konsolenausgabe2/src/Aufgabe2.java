
public class Aufgabe2 {

	public static void main(String[] args) {
		
		//Definition der Variablen
		
		String a = "0!" ;		//Fakult�t
		String b = "1!" ;
		String c = "2!" ;
		String d = "3!" ;
		String e = "4!" ;
		String f = "5!" ;
		
		String g = " " ;		//Ergebnis
		String h = "1" ;
		String i = "2" ;
		String j = "6" ;
		String k = "24" ;
		String l = "120" ;
		
		//Ausgaben
		
		System.out.printf("%-4s =", a) ;
		System.out.printf("%-18s =", g) ;
		System.out.printf("%4s%n", h) ;
		
		System.out.printf("%-4s =", b) ;
		System.out.printf(" 1%-17s=", g) ;
		System.out.printf("%4s%n", h) ;
		
		System.out.printf("%-4s =", c) ;
		System.out.printf(" 1 * 2%-13s=", g) ;
		System.out.printf("%4s%n", i) ;
		
		System.out.printf("%-4s =", d) ;
		System.out.printf(" 1 * 2 * 3%-9s=", g) ;
		System.out.printf("%4s%n", j) ;
		
		System.out.printf("%-4s =", e) ;
		System.out.printf(" 1 * 2 * 3 * 4%-5s=", g) ;
		System.out.printf("%4s%n", k) ;
		
		System.out.printf("%-4s =", f) ;
		System.out.printf(" 1 * 2 * 3 * 4 * 5%-1s=", g) ;
		System.out.printf("%4s%n", l) ;

	}

}
